module Formulario
  class Field
    TYPES = {
      String      => Formulario::Field::String,
      :string     => Formulario::Field::String,
      Integer     => Formulario::Field::Integer,
      :integer    => Formulario::Field::Integer,
      Boolean     => Formulario::Field::Boolean,
      :boolean    => Formulario::Field::Boolean,
      Time        => Formulario::Field::Time,
      :time       => Formulario::Field::Time,
      Date        => Formulario::Field::Date,
      :date       => Formulario::Field::Date,
      Collection  => Formulario::Field::Collection,
      :collection => Formulario::Field::Collection,
      :array      => Formulario::Field::Collection,
    }

    def self.type_for(type)
      TYPES.fetch(type)
    end

    attr_reader :raw_value

    def self.for(raw_value)
      if raw_value.is_a?(Field)
        raw_value
      elsif ::Formulario::Utils.empty?(raw_value)
        default
      else
        build(raw_value)
      end
    end

    def self.type_for(field_type)
      type = TYPES.fetch(field_type) { field_type }
      if type < ::Formulario::Form
        FormField[type]
      else
        type
      end
    end

    def self.default
      Blank.new
    end

    def exceptional?
      false
    end

    def blank?
      false
    end

    def value
      raw_value
    end

    def to_s
      value.to_s
    end
    alias :to_str :to_s

    def inspect
      "#{self.class}[#{to_s}]"
    end

    def ==(other)
      new_other = Field.for(other)
      value == new_other.value
    end

    def exceptional_class
      ::Formulario::Field::ExceptionalValue
    end

    private

    def self.build(raw_value)
      new(raw_value)
    end

    def initialize(raw_value)
      @raw_value = raw_value
      freeze
    end
  end
end
