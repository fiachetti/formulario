module Formulario
  class Field
    class Blank < Field
      def initialize(raw_value=nil)
        super(raw_value)
      end

      def blank?
        true
      end
    end
  end
end
