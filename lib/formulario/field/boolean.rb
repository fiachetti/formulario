module Formulario
  class Field
    class Boolean < Formulario::Field
      private

      def self.build(raw_value)
        case get_value(raw_value)
        when true, 'true', 't', '1'
          new true
        when false, 'false', 'f', '0', ''
          new false
        else
          Formulario::Field::ExceptionalValue.new(raw_value, reasons: ['needs to be boolean'])
        end
      end

      def self.get_value(raw_value)
        raw_value.to_s.downcase
      end

      def self.default
        new(false)
      end
    end
  end
end
