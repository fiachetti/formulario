module Formulario
  class Field
    class Collection < Formulario::Field
      include Enumerable

      def self.[](type)
        Class.new(self).tap { |typed_class|
          typed_class.fields_type = type
        }
      end

      def fields
        raw_value
      end

      def exceptional?
        fields.any?(&:exceptional?)
      end

      def each(&block)
        fields.each(&block)
      end

      def values
        map(&:value)
      end

      private

      def self.build(raw_values)
        new( Array(raw_values).map { |raw_value|
               fields_type.for(raw_value)
             }
           )
      end

      def self.fields_type
        @fields_type ||= Field
      end

      def self.fields_type=(type)
        @fields_type = type_for(type)
      end
    end
  end
end
