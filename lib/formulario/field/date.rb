module Formulario
  class Field
    class Date < DefaultTimeField

      private

      def self.base_class
        ::Date
      end

      def self.allowed_keys
        %i[ year month day ]
      end

      def self.parse_hash(hash)
        return invalid_keys_exceptional_value(hash) if has_invalid_keys?(hash)

        now = ::Date.today
        new ::Date.new(
          hash[:year]  || now.year,
          hash[:month] || now.month,
          hash[:day]   || now.day,
        )
      end
    end
  end
end
