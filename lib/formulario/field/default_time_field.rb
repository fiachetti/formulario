require "time"

module Formulario
  class Field
    class DefaultTimeField < Formulario::Field
      private

      def self.build(raw_value)
        case raw_value
        when base_class
          new raw_value
        when ::Hash
          parse_hash(raw_value)
        when ::String
          new base_class.parse(raw_value)
        end
      end

      def self.invalid_keys_exceptional_value(hash)
        ExceptionalValue.new(hash,
                             reasons: [
                               "the only possible keys are: #{allowed_keys.map(&:inspect).join(', ')}"
                             ])
      end

      def self.has_invalid_keys?(hash)
        hash.keys.any? {|key| !allowed_keys.include?(key) }
      end

    end
  end
end
