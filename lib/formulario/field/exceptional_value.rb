module Formulario
  class Field
    class ExceptionalValue < Formulario::Field
      include Enumerable

      attr_reader :reasons

      def self.for(raw_value, reasons: Undefined)
        new(raw_value, reasons: reasons)
      end

      def exceptional?
        true
      end

      def inspect
        "#{self.class}[#{value} (#{reasons.join(' | ')})]"
      end

      def to_a
        reasons
      end

      def each(&block)
        reasons.each(&block)
      end

      def exceptional_class
        self.class
      end

      private

      def initialize(raw_value, reasons: Undefined)
        the_reasons = reasons == Undefined ? ["is invalid"] : Array(reasons)

        if raw_value.is_a?(self.class)
          new_reasons = raw_value.reasons.concat(Array(the_reasons))
          value       = raw_value.raw_value
        else
          new_reasons = Array(the_reasons)
          value       = raw_value
        end

        @reasons = new_reasons

        super(value)
      end
    end
  end
end
