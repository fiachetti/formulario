module Formulario
  class Field
    class Float < Formulario::Field
      private

      def self.build(raw_value)
        new Float(raw_value)
      rescue ArgumentError, TypeError => e
        Formulario::Field::ExceptionalValue.new(raw_value, reasons: ['needs to represent a float'])
      end

    end
  end
end
