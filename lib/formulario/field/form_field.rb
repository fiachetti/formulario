module Formulario
  class Field
    class FormField < Formulario::Field
      attr_reader :form

      def self.[](type)
        Class.new(self) do |typed_class|
          typed_class.type = type
        end
      end

      def self.type
        @type
      end

      def self.default
        new(type.default)
      end

      def value
        form.params
      end

      def exceptional?
        ! form.valid?
      end

      def fields
        form.fields
      end

      def [](value)
        fields[value]
      end

      def initialize(raw_value)
        @form = if raw_value.is_a?(::Formulario::Form)
                  raw_value
                else
                  self.class.type.new(**raw_value)
                end

        super
      end

      private

      def self.build(raw_value)
        raise UndefinedFormFieldType.new unless @type

        new(raw_value)
      end

      def self.type=(new_type)
        @type = new_type
      end

      def method_missing(name, *args, &block)
        return fields[name] if fields.has_key?(name)

        super
      end

      UndefinedFormFieldType = Class.new(RuntimeError)
    end
  end
end
