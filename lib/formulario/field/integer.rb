module Formulario
  class Field
    class Integer < Formulario::Field
      private

      def self.build(raw_value)
        new Integer(raw_value)
      rescue ArgumentError, TypeError => e
        ExceptionalValue.new(raw_value, reasons: ['needs to represent an integer'])
      end

    end
  end
end
