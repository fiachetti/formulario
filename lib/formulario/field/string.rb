module Formulario
  class Field
    class String < Formulario::Field
      private

      def self.build(raw_value)
        new String(raw_value)
      rescue TypeError => e
        ExceptionalValue.new(raw_value, reasons: ['needs to represent a string'])
      end

      def initialize(raw_value)
        super
      end
    end
  end
end
