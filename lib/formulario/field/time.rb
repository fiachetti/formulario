module Formulario
  class Field
    class Time < DefaultTimeField

      private

      def self.base_class
        ::Time
      end

      def self.allowed_keys
        %i[ year month day hour minute second timezone ]
      end

      def self.parse_hash(hash)
        return invalid_keys_exceptional_value(hash) if has_invalid_keys?(hash)

        now = ::Time.now
        new ::Time.new(
          hash[:year]     || now.year,
          hash[:month]    || now.month,
          hash[:day]      || now.day,
          hash[:hour]     || now.hour,
          hash[:minute]   || now.min,
          hash[:second]   || now.sec,
          hash[:timezone] || now.zone,
        )
      end
    end
  end
end
