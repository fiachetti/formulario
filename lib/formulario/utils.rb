module Formulario
  module Utils
    module_function

    def empty?(object)
      object.nil? || object.to_s.strip.empty?
    end
  end
end
