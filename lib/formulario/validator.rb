module Formulario
  class Validator

    def initialize(&validation_block)
      @validation_block = validation_block if block_given?
    end

    def call(value:, field_name: nil, object: nil)
      @value      = value
      @field_name = field_name
      @object     = object
      @valid      = object.instance_exec(value, &validation_block)
      @validated  = true

      self
    end

    def to_proc
      method(:call)
    end

    def valid?
      raise ValidationNotPerformed unless validated?

      valid
    end

    def message
      valid? ? '' : failure_text.to_s
    end

    def on_valid
      yield if valid?

      self
    end

    def on_invalid
      yield unless valid?

      self
    end

    private

    attr_reader :value
    attr_reader :field_name
    attr_reader :object

    attr_reader :valid
    attr_reader :validated
    alias :validated? :validated

    def validate(value)
      true
    end

    def validation_block
      @validation_block ||= method(:validate)
    end


    def failure_text
      'is invalid'
    end

    ValidationNotPerformed = Class.new(RuntimeError)
  end
end
