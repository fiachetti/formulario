RSpec.describe Formulario::Field::Blank do
  it "is not exceptional" do
    expect(Formulario::Field::Blank.new.exceptional?).to eql(false)
  end

  it "returns an empty string as to_s" do
    blank = Formulario::Field::Blank.new

    expect(blank.to_s).to eql("")
    expect(blank.raw_value).to eql(nil)
    expect(blank.value).to eql(nil)
  end

  it "can be ititialized with square brackets" do
    blank = Formulario::Field::Blank.for(' \t ')

    expect(blank.to_s).to eql(' \t ')
  end

  it "is blank" do
    blank = Formulario::Field::Blank.new

    expect(blank.blank?).to eql(true)
  end
end
