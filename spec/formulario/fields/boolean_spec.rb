RSpec.describe Formulario::Field::Boolean do
  it "returns true when passing true in" do
    field = Formulario::Field::Boolean.for(true)

    expect(field.value).to eql(true)
  end

  it "returns false when passing a false in" do
    field = Formulario::Field::Boolean.for(false)

    expect(field.value).to eql(false)
  end

  it "coerces the 'true' string" do
    field = Formulario::Field::Boolean.for('true')

    expect(field.value).to eql(true)
  end

  it "coerces the 'false' string" do
    field = Formulario::Field::Boolean.for('false')

    expect(field.value).to eql(false)
  end

  it "coerces the 'tRuE' string" do
    field = Formulario::Field::Boolean.for('tRuE')

    expect(field.value).to eql(true)
  end

  it "coerces the 'FAlsE' string" do
    field = Formulario::Field::Boolean.for('FAlsE')

    expect(field.value).to eql(false)
  end

  it "coerces the 't' string" do
    field = Formulario::Field::Boolean.for('t')

    expect(field.value).to eql(true)
  end

  it "coerces the 'f' string" do
    field = Formulario::Field::Boolean.for('f')

    expect(field.value).to eql(false)
  end

  it "coerces the 'T' string" do
    field = Formulario::Field::Boolean.for('T')

    expect(field.value).to eql(true)
  end

  it "coerces the 'F' string" do
    field = Formulario::Field::Boolean.for('F')

    expect(field.value).to eql(false)
  end

  it "coerces 1" do
    field = Formulario::Field::Boolean.for(1)

    expect(field.value).to eql(true)
  end

  it "coerces 0" do
    field = Formulario::Field::Boolean.for(0)

    expect(field.value).to eql(false)
  end

  it "coerces '1'" do
    field = Formulario::Field::Boolean.for('1')

    expect(field.value).to eql(true)
  end

  it "coerces '0'" do
    field = Formulario::Field::Boolean.for('0')

    expect(field.value).to eql(false)
  end

  it "coerces the empty string to false" do
    field = Formulario::Field::Boolean.for('')

    expect(field.value).to eql(false)
  end

  it "coerces nil to false" do
    field = Formulario::Field::Boolean.for(nil)

    expect(field.value).to eql(false)
  end


  it "returns an ExceptionalValue if the value can't be coerced into a Formulario::Field::Boolean" do
    field = Formulario::Field::Boolean.for('not a boolean')

    expect(field.exceptional?).to eql(true)
    expect(field.reasons).to eql(['needs to be boolean'])
  end
end
