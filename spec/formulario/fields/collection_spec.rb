RSpec.describe Formulario::Field::Collection do
  it "returns an array when passing an array in" do
    field = Formulario::Field::Collection.for([])

    expect(field.fields).to eql([])
  end

  it "returns a collection even when passing a single value" do
    field = Formulario::Field::Collection.for(123)

    expect(field.fields.first.class).to eql(Formulario::Field)
  end

  it "converts all of it's fields to fields" do
    field = Formulario::Field::Collection.for([123, 456])

    expect(field.fields.map(&:class)).to eql([Formulario::Field, Formulario::Field])
    expect(field.fields.map(&:value)).to eql([123, 456])
  end

  it "can have a type" do
    field = Formulario::Field::Collection[Formulario::Field::Integer].for(123)

    expect(field.fields.first.class).to eql(::Formulario::Field::Integer)
  end

  it "can accept a symbol as the type" do
    field = Formulario::Field::Collection[:string].for(123)

    expect(field.fields.first.class).to eql(::Formulario::Field::String)
  end

  it "is exceptional if any of it's fields is exceptional" do
    field = Formulario::Field::Collection.for([123, Formulario::Field::ExceptionalValue.for('bad value'), 456])

    expect(field.exceptional?).to eql(true)
  end

  it "is enumerable and delegates each to the fields" do
    field = Formulario::Field::Collection.for([123, '456'])

    expect(field).to be_a(Enumerable)
    expect(field.map(&:to_s)).to eql(%w[123 456])
  end

  it "returns an array of values" do
    field = Formulario::Field::Collection[Formulario::Field::Integer].for([123, '456'])

    expect(field.values).to eql([123, 456])
  end

  it "accepts a Form as a field", :preserve_constants do
    ::MyForm = Class.new(Formulario::Form) do
      field :my_field
    end

    field = Formulario::Field::Collection[::MyForm].for([
                                                          { my_field: 'VALUE' },
                                                        ])


    expect(field.first).to be_a Formulario::Field::FormField
    expect(field.first.my_field).to eq('VALUE')
  end

  it "accepts a Form as a field value", :preserve_constants do
    ::MyForm = Class.new(Formulario::Form) do
      field :my_field
    end

    field = Formulario::Field::Collection[::MyForm].for(
      [
        ::MyForm.new(my_field: 'VALUE'),
      ]
    )


    expect(field.first).to be_a Formulario::Field::FormField
    expect(field.first.my_field).to eq('VALUE')
  end
end
