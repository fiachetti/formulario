RSpec.describe Formulario::Field::Date do
  let(:today) { ::Date.new(2009, 8, 7) }

  it "returns a Date when passing a Date in" do
    field = Formulario::Field::Date.for(::Date.new(2019, 12, 6))

    expect(field.value).to eql(::Date.new(2019, 12, 6))
  end

  it "returns a Date when passing an appropriate Hash in" do
    field = Formulario::Field::Date.for(
      {
        year:     2019,
        month:    10,
        day:      15,
      }
    )

    expect(field.value).to eql(::Date.new(2019, 10, 15))
  end

  it "defaults to time.today when passing an empty hash" do
    allow(::Date).to receive(:today).and_return(today)
    field = Formulario::Field::Date.for( {} )

    expect(field.value).to eql(today)
  end

  it "defaults to time.today when passing an empty hash and overwriting keys" do
    allow(::Date).to receive(:today).and_return(::Date.new(2009, 8, 7))
    field = Formulario::Field::Date.for(
      {
        year: 2019,
      }
    )

    expect(field.value).to eql(::Date.new(2019, 8, 7))
  end

  it "returns an ExceptionalValue when passing a hash with bad keys" do
    field = Formulario::Field::Date.for({ year: 2019, hello: 3 })

    expect(field.exceptional?).to eql(true)
    expect(field.reasons).to eql(['the only possible keys are: :year, :month, :day'])
  end

  it "coerces a string" do
    field = Formulario::Field::Date.for("2009-08-07")

    expect(field.value).to eql(today)
  end
end
