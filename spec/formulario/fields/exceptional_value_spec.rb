RSpec.describe Formulario::Field::ExceptionalValue do
  it "is exceptional" do
    expect(Formulario::Field::ExceptionalValue.new('val').exceptional?).to eql(true)
  end

  it "has a default reason" do
    v = Formulario::Field::ExceptionalValue.new(3)

    expect(v.reasons).to eql(["is invalid"])
  end

  it "defaults Undefined reason" do
    v = Formulario::Field::ExceptionalValue.new(3, reasons: Formulario::Undefined)

    expect(v.reasons).to eql(["is invalid"])
  end

  it "allows to set a reason" do
    v = Formulario::Field::ExceptionalValue.new(1, reasons: 'reason')

    expect(v.reasons).to eql(['reason'])
  end

  it "allows more than one reason" do
    v = Formulario::Field::ExceptionalValue.new(1, reasons: ['r1', 'r2'])

    expect(v.reasons).to eql(['r1', 'r2'])
  end

  it "concats the reasons array if raw_value is an Formulario::Field::ExceptionalValue" do
    v1 = Formulario::Field::ExceptionalValue.new(1, reasons: ['r1', 'r2'])

    v2 = Formulario::Field::ExceptionalValue.new(v1, reasons: 'new reason')

    expect(v2.reasons).to eql(['r1', 'r2', 'new reason'])
  end

  it "concats the reasons array if raw_value is an Formulario::Field::ExceptionalValue if passing an array in" do
    v1 = Formulario::Field::ExceptionalValue.new(1, reasons: ['r1', 'r2'])

    v2 = Formulario::Field::ExceptionalValue.new(v1, reasons: ['n1', 'n2'])

    expect(v2.reasons).to eql(['r1', 'r2', 'n1', 'n2'])
  end

  it "converts to an array" do
    v = Formulario::Field::ExceptionalValue.new(nil, reasons: ['r1', 'r2'])

    expect(v.map(&:upcase)).to eql(['R1', 'R2'])
  end
end
