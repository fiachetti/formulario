RSpec.describe Formulario::Field do
  it "is not exceptional" do
    expect(Formulario::Field.for(nil).exceptional?).to eql(false)
  end

  it "[] is indempotent" do
    nested  = Formulario::Field.for(1)
    wrapper = Formulario::Field.for(nested)

    expect(nested).to eql(nested)
  end

  it "delegates #to_s to the raw_value" do
    v = Formulario::Field.for(123)

    expect(v.to_s).to eql("123")
  end

  it "returns the raw value as the value by default" do
    v = Formulario::Field.for(Time.new(2019, 8, 10))

    expect(v.value).to eql(Time.new(2019, 8, 10))
  end

  it "is not blank" do
    v = Formulario::Field.for('test')

    expect(v.blank?).to eql(false)
  end

  context "equality" do
    it "is equal to a Formulario::Field with the same value" do
      v = Formulario::Field.for('test')

      expect(v == Formulario::Field.for('test')).to eql(true)
    end

    it "is not equal to a Formulario::Field with another value" do
      v = Formulario::Field.for('test')

      expect(v == Formulario::Field.for(nil)).to eql(false)
    end

    it "can be compared with a non Formulario::Field" do
      v = Formulario::Field.for('test')

      expect(v == 'test').to eql(true)
    end

    context 'validations' do

      it "is valid if it passes validations" do
        form_class = Class.new(Formulario::Form) do
          field :int

          validate :int do |val|
            val > 45
          end
        end

        form = form_class.new(int: 150)

        expect(form.valid?).to eql(true)
      end

      # xit "is invalid if it doesn't pass validations" do
      #   form_class = Class.new(Form) do
      #     field :int

      #     validate :int do |val|
      #       val > 45
      #     end
      #   end

      #   form = form_class.new(int: 1)

      #   expect(form.valid?).to eql(false)
      #   expect(form.int.reasons).to eql(['is invalid'])
      # end

      # xit "executes validations on the instance", :preserve_constants do
      #   ::MyForm = Class.new(Form) do
      #     field :int
      #     attr_accessor :base

      #     validate :int do |val|
      #       val > form.base
      #     end
      #   end

      #   form1 = MyForm.new(int: 1, base: 2)
      #   expect(form1.int.exceptional?).to eql(true)

      #   form2 = MyForm.new(int: 5, base: 2)
      #   expect(form2.int.exceptional?).to eql(false)
      # end

      # xit "allows to set a message" do
      #   form_class = Class.new(Form) do
      #     field :int

      #     validate :int, message: 'too small' do |val|
      #       val > 45
      #     end
      #   end

      #   form = form_class.new(int: 1)

      #   expect(form.int.reasons).to eql(['too small'])
      # end

      # xit "allows multiple validations on the same field" do
      #   form_class = Class.new(Form) do
      #     field :int

      #     validate :int, message: 'too small' do |val|
      #       val > 45
      #     end

      #     validate :int, message: 'too very small' do |val|
      #       val > 50
      #     end
      #   end

      #   form = form_class.new(int: 1)

      #   expect(form.int.reasons).to eql(['too small', 'too very small'])
      # end

      # xit "allows multiple validations on different fields" do
      #   form_class = Class.new(Form) do
      #     field :int
      #     field :str

      #     validate :int, message: 'too small' do |val|
      #       val > 45
      #     end

      #     validate :str, message: 'needs to start with the letter m' do |val|
      #       val.start_with?('m')
      #     end

      #     validate :str, message: 'needs to end with the letter m' do |val|
      #       val.end_with?('m')
      #     end
      #   end

      #   form = form_class.new(int: 1, str: 'master')

      #   expect(form.int.reasons).to eql(['too small'])
      #   expect(form.str.reasons).to eql(['needs to end with the letter m'])
      # end

      # xit "takes symbols as method names" do
      #   field_class = Class.new(Formulario::Formulario::Field::Integer) do
      #     def is_bigger(val)
      #       val > 45
      #     end
      #   end

      #   form_class = Class.new(Form) do
      #     field :int, field_class

      #     validate :int, :is_bigger, message: 'needs to be greater than 45'

      #   end

      #   form = form_class.new(int: 1)

      #   expect(form.int.reasons).to eql(['needs to be greater than 45'])
      # end

      # xit "accepts a validator", :preserve_constants do
      #   ::LenghtValidator = Class.new(Validator) do
      #     attr_reader :length

      #     def initialize(length)
      #       @length = length
      #       super()
      #     end

      #     def validate(value)
      #       value.size == length
      #     end

      #     def failure_text
      #       value.size > length ? 'too large' : 'too short'
      #     end
      #   end

      #   form_class = Class.new(Form) do
      #     field :str

      #     validate :str, LenghtValidator.new(5)
      #   end

      #   form = form_class.new(str: 'a')

      #   expect(form.valid?).to eql(false)
      #   expect(form.str.exceptional?).to eql(true)
      #   expect(form.str.reasons).to eql(['too short'])
      # end
    end

  end
end
