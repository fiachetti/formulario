RSpec.describe Formulario::Field::Float do
  it "returns an float when passing an float in" do
    field = Formulario::Field::Float.for(123.4)

    expect(field.value).to eql(123.4)
  end

  it "returns an float when passing an integer in" do
    field = Formulario::Field::Float.for(123)

    expect(field.value).to eql(123.0)
  end

  it "coerces a string" do
    field = Formulario::Field::Float.for('345.67')

    expect(field.value).to eql(345.67)
  end

  it "returns an ExceptionalValue if the value can't be coerced into an Formulario::Field::Float" do
    field = Formulario::Field::Float.for('one point 2')

    expect(field.exceptional?).to eql(true)
    expect(field.reasons).to eql(['needs to represent a float'])
  end

  it "returns an ExceptionalValue if the value is a bare object" do
    field = Formulario::Field::Float.for(Object.new)

    expect(field.exceptional?).to eql(true)
    expect(field.reasons).to eql(['needs to represent a float'])
  end
end
