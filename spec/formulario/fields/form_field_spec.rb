RSpec.describe Formulario::Field::FormField do
  it "has a type", :preserve_constants do
    ::NestedForm = Class.new(Formulario::Form
                            )
    expect { Formulario::Field::FormField[NestedForm].for({}) }.not_to raise_error
  end

  it "has a type" do
    expect { Formulario::Field::FormField.for({}) }
      .to raise_error(::Formulario::Field::FormField::UndefinedFormFieldType)
  end

  it "scopes the type only to the newly created file", :preserve_constants do
    ::NestedForm1 = Class.new(Formulario::Form)
    ::NestedForm2 = Class.new(Formulario::Form)

    field_class1 = Formulario::Field::FormField[NestedForm1]
    field_class2 = Formulario::Field::FormField[NestedForm2]

    expect(field_class1.type).to eql(NestedForm1)
    expect(field_class2.type).to eql(NestedForm2)
  end

  it "returns an hash when passing a hash in", :preserve_constants do
    ::NestedForm = Class.new(Formulario::Form) do
      field :str, default: "DEFAULT"
    end

    field = Formulario::Field::FormField[NestedForm].for({})

    expect(field.value).to eql({str: "DEFAULT"})
  end

  it "initializes the underlying form", :preserve_constants do
    ::NestedForm = Class.new(Formulario::Form) do
      field :str, :string
      field :int, :integer
    end

    field = Formulario::Field::FormField[NestedForm].for({int: 123})

    expect(field.value).to eql({str: nil, int: 123})
  end

  it "is exceptional if any of the nested form's values is exceptional", :preserve_constants do
    ::NestedForm = Class.new(Formulario::Form) do
      field :f1
      field :f2

      validate :f1 do
        true
      end

      validate :f2 do
        false
      end
    end

    field = Formulario::Field::FormField[NestedForm].for({})

    expect(field.exceptional?).to eql(true)
  end

  it "returns the underlying form", :preserve_constants do
    ::NestedForm = Class.new(Formulario::Form) do
      field :f1
      field :f2
    end

    field = Formulario::Field::FormField[NestedForm].for({})

    expect(field.form).to be_a(NestedForm)
    expect(field.form.fields.keys).to eql([:f1, :f2])
  end

  it "delegates fields to the underlying form", :preserve_constants do
    ::NestedForm = Class.new(Formulario::Form) do
      field :f1
      field :f2

      validate :f1 do
        true
      end

      validate :f2 do
        false
      end

    end

    field = Formulario::Field::FormField[NestedForm].for({f1: 1, f2: 2})

    expect(field.fields.keys).to eql([:f1, :f2])
    expect(field.fields[:f1]).to eq(Formulario::Field.for(1))
    expect(field.fields[:f2]).to eq(Formulario::Field::ExceptionalValue.for(2))
  end

  it "delegates the [] methodo to the  form", :preserve_constants do
    ::NestedForm = Class.new(Formulario::Form) do
      field :f1
      field :f2

      validate :f1 do
        true
      end

      validate :f2 do
        false
      end

    end

    field = Formulario::Field::FormField[NestedForm].for({f1: 1, f2: 2})

    expect(field[:f1]).to eq(Formulario::Field.for(1))
    expect(field[:f2]).to eq(Formulario::Field::ExceptionalValue.for(2))
  end

  it "delegates each field to the form", :preserve_constants do
    ::NestedForm = Class.new(Formulario::Form) do
      field :f1
      field :f2

      validate :f1 do
        true
      end

      validate :f2 do
        false
      end

    end

    field = Formulario::Field::FormField[NestedForm].for({f1: 1, f2: 2})

    expect(field.f1).to eq(Formulario::Field.for(1))
    expect(field.f2).to eq(Formulario::Field::ExceptionalValue.for(2))
  end

end
