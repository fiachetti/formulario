RSpec.describe Formulario::Field::Integer do
  it "returns an integer when passing an integer in" do
    field = Formulario::Field::Integer.for(123)

    expect(field.value).to eql(123)
  end

  it "coerces a string" do
    field = Formulario::Field::Integer.for('345')

    expect(field.value).to eql(345)
  end

  it "returns an ExceptionalValue if the value can't be coerced into an Integer" do
    field = Formulario::Field::Integer.for('not even 1')

    expect(field.exceptional?).to eql(true)
    expect(field.reasons).to eql(['needs to represent an integer'])
  end

  it "returns an ExceptionalValue if the value is a bare object" do
    field = Formulario::Field::Integer.for(Object.new)

    expect(field.exceptional?).to eql(true)
    expect(field.reasons).to eql(['needs to represent an integer'])
  end
end
