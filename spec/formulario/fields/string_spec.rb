RSpec.describe Formulario::Field::String do
  it "returns a string when passing a string in" do
    field = Formulario::Field::String.for('a string')

    expect(field.value).to eql('a string')
  end

  it "coerces an string if passing an integer" do
    field = Formulario::Field::String.for(123)

    expect(field.value).to eql('123')
  end
end
