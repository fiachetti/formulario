RSpec.describe Formulario::Field::Time do
  let(:now) { ::Time.new(2009, 8, 7, 6, 5, 4, '-03:00') }

  it "returns a Time when passing a Time in" do
    field = Formulario::Field::Time.for( ::Time.new(2019, 12, 6) )

    expect(field.value).to eql(::Time.new(2019, 12, 6))
  end

  it "returns a Time when passing an appropriate Hash in" do
    field = Formulario::Field::Time.for( {
                        year:     2019,
                        month:    10,
                        day:      15,
                        hour:     23,
                        minute:   11,
                        second:   9,
                        timezone: '-03:00',
                      } )

    expect(field.value).to eql(::Time.new(2019, 10, 15, 23, 11, 9, '-03:00'))
  end

  it "defaults to time.now when passing an empty hash" do
    allow(::Time).to receive(:now).and_return(now)
    field = Formulario::Field::Time.for( {} )

    expect(field.value).to eql(now)
  end

  it "defaults to time.now when passing an empty hash and overwriting keys" do
    allow(::Time).to receive(:now).and_return(::Time.new(2009, 8, 7, 6, 5, 4, '-03:00'))
    field = Formulario::Field::Time.for( {
                        year: 2019,
                        day:  17,
                      } )

    expect(field.value).to eql(::Time.new(2019, 8, 17, 6, 5, 4, '-03:00'))
  end

  it "returns an ExceptionalValue when passing a hash with bad keys" do
    field = Formulario::Field::Time.for({year: 2019, hello: 3})

    expect(field.exceptional?).to eql(true)
    expect(field.reasons).to eql(['the only possible keys are: :year, :month, :day, :hour, :minute, :second, :timezone'])
  end

  it "coerces a string" do
    field = Formulario::Field::Time.for( "2009-08-07 06:05:04 -0300" )

    expect(field.value).to eql(now)
  end
end
