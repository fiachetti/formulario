RSpec.describe Formulario::Form do
    it "is initialized with a hash of parameters" do
      form_class = Class.new(Formulario::Form) do
        attr_accessor :example
      end

      form = form_class.new(example: :example)

      expect(form.example).to eql(:example)
    end

    context ".for" do
      it "initializes the form with the attributes from the model" do
        form_class = Class.new(Formulario::Form) do
          field :str, Formulario::Field::String
          field :int, Formulario::Field::Integer
        end

        model_class = Struct.new(:str, :int)

        form = form_class.for(model_class.new('EXAMPLE', 3))

        expect(form.params).to eql({ str: 'EXAMPLE', int: 3 })
      end

      it "ignores nil model" do
        form_class = Class.new(Formulario::Form) do
          field :str, Formulario::Field::String
        end

        form = form_class.for(nil, str: "STR")

        expect(form.str).to eq("STR")
      end

      it "allows other input" do
        form_class = Class.new(Formulario::Form) do
          field :str, Formulario::Field::String

          attr_accessor :persisted

          def persisted?
            !! persisted
          end
        end

        model_class = Struct.new(:str, :persisted?)
        model = model_class.new('EXAMPLE', :persisted)

        form = form_class.for(model, persisted: model.persisted?)

        expect(form.persisted?).to eql(true)
      end

      it "saves the model" do
        form_class = Class.new(Formulario::Form) do
          field :str, Formulario::Field::String
        end

        model_class = Struct.new(:str)
        model = model_class.new('EXAMPLE')

        form = form_class.for(model)

        expect(form.model).to eql(model)
      end

      it "overides model fields" do
        form_class = Class.new(Formulario::Form) do
          field :str, Formulario::Field::String

          attr_accessor :persisted

          def persisted?
            !! persisted
          end
        end

        model_class = Struct.new(:str)
        model = model_class.new('EXAMPLE')

        form = form_class.for(model, str: '')

        expect(form.str).to eq('')
      end
    end

    it "sets attribute to be Field by default" do
      form_class = Class.new(Formulario::Form) do
        field :example
      end

      form = form_class.new(example: :example)

      expect(form.example).to eq(Formulario::Field.for(:example))
    end

    it "can exist multiple instances", :preserve_constants do
      form_class = Class.new(Formulario::Form) do
        field :name
      end

      alice = form_class.new(name: 'ALICE')
      bob   = form_class.new(name: 'BOB')

      expect(alice.name.value).to eql('ALICE')
      expect(bob.name.value).to   eql('BOB')
    end

    it "field setting is indempotent" do
      form_class = Class.new(Formulario::Form) do
        field :example
      end

      form = form_class.new(example: Formulario::Field.for(:example))

      expect(form.example).to eq(Formulario::Field.for(:example))
    end

    it "sets a field to blank if it's nil" do
      form_class = Class.new(Formulario::Form) do
        field :example
      end

      form = form_class.new(example: nil)

      expect(form.example).to eq(::Formulario::Field::Blank.new)
    end

    it "sets a field to blank if it's an empty string" do
      form_class = Class.new(Formulario::Form) do
        field :example
      end

      form = form_class.new(example: " \n \t ")

      expect(form.example).to eq(::Formulario::Field::Blank.new)
    end

    it "allows the user to set a field" do
      form_class = Class.new(Formulario::Form) do
        field :double

        def set_double(integer)
          Formulario::Field.for(integer * 2)
        end
      end

      form = form_class.new(double: 3)

      expect(form.double).to eq(Formulario::Field.for(6))
    end

    it "allows the user to set a field and converts it to a whole value" do
      form_class = Class.new(Formulario::Form) do
        field :triple

        def set_triple(integer)
          integer * 3
        end
      end

      form = form_class.new(triple: 5)

      expect(form.triple).to eq(Formulario::Field.for(15))
    end

    it "allows to define a default field type", :preserve_constants do
      ::MyField = Class.new(Formulario::Field) do
        attr_reader :value

        def initialize(value)
          @value = value
        end
      end

      form_class = Class.new(Formulario::Form) do
        field :my_field, MyField
      end

      form = form_class.new(my_field: 'a string')

      expect(form.my_field).to eq(Formulario::Field.for(MyField.new('a string')))
    end

    it "uses the square brackets initializer if it's defined on the type class", :preserve_constants do
      ::MyField = Class.new(Formulario::Field)

      form_class = Class.new(Formulario::Form) do
        field :my_field, MyField
      end

      form = form_class.new(my_field: 'a-slug')

      expect(form.my_field).to eq(Formulario::Field.for('a-slug'))
    end

    context "field types" do
      it "coerces strings" do
        form_class = Class.new(Formulario::Form) do
          field :str1, :string
          field :str2, Formulario::Field::String
        end

        value = 1
        form  = form_class.new(str1: value, str2: value)

        expect(form.str1).to eq('1')
        expect(form.str2).to eq('1')
      end

      it "returns an exceptional field if it can't be converted" do
        form_class = Class.new(Formulario::Form) do
          field :int, :integer
        end

        form  = form_class.new(int: Object.new)

        expect(form.int.exceptional?).to eql(true)
        expect(form.int.reasons).to eql(["needs to represent an integer"])
      end

      it "coerces integers" do
        form_class = Class.new(Formulario::Form) do
          field :int1, :integer
          field :int2, Formulario::Field::Integer
        end

        value = '1'
        form  = form_class.new(int1: value, int2: value)

        expect(form.int1).to eq(1)
        expect(form.int2).to eq(1)
      end

      it "is exceptional if it can't be converted to an integer" do
        form_class = Class.new(Formulario::Form) do
          field :int1, :integer
          field :int2, :integer
        end

        value = Object.new
        form  = form_class.new(int1: value, int2: value)

        expect(form.int1.exceptional?).to eql(true)
        expect(form.int1.reasons).to eql(['needs to represent an integer'])

        expect(form.int2.exceptional?).to eql(true)
        expect(form.int2.reasons).to eql(['needs to represent an integer'])
      end

      it "accepts collection fields" do
        form_class = Class.new(Formulario::Form) do
          field :list, Formulario::Field::Collection[Formulario::Field::String]
        end

        form = form_class.new(list: %w[foo bar])

        expect(form.list).to match_array([
                                           Formulario::Field::String.new('foo'),
                                           Formulario::Field::String.new('bar'),
                                         ])
      end

      it "accepts a collection of form fields", :preserve_constants do
        ::Lesson = Struct.new(:title) do
        end


        ::LessonForm = Class.new(Formulario::Form) do
          field :title
        end

        ::CourseForm = Class.new(Formulario::Form) do
          field :list, Formulario::Field::Collection[::LessonForm]
        end

        form = ::CourseForm.new(list: [
                                  ::LessonForm.new(title: 'Lesson 1'),
                                ])

        expect(form.list.first).to be_a(::Formulario::Field::FormField)
        expect(form.list.first.title).to eq('Lesson 1')
      end

    end

    context "fields" do
      it "starts out as an empty hash" do
        form = Formulario::Form.new

        expect(form.fields).to eq({})
      end

      it "has all the initialized fields", :preserve_constants do
        ::Slug = Class.new(Formulario::Field)

        form_class = Class.new(Formulario::Form) do
          field :name, :string
          field :slug, Slug
        end

        form = form_class.new(name: 'Pepe', slug: 'the-slug')

        expect(form.fields).to eq({
                                    name: Formulario::Field.for('Pepe'),
                                    slug: Slug.for('the-slug'),
                                  })
      end

      it "defaults fields to Blank", :preserve_constants do
        ::Slug = Class.new(Formulario::Field)

        form_class = Class.new(Formulario::Form) do
          field :name, :string
          field :slug, Slug
        end

        form = form_class.new(slug: 'the-slug')

        expect(form.fields).to eq({
                                    name: ::Formulario::Field::Blank.new,
                                    slug: Slug.for('the-slug'),
                                  })
      end
    end

    context "defaults" do
      it "accept default values for fields" do
        form_class = Class.new(Formulario::Form) do
          field :str, :string, default: 'THE DEFAULT STRING'
        end

        form = form_class.new

        expect(form.str).to eq(Formulario::Field.for('THE DEFAULT STRING'))
      end

      it "accepts a proc as the default and runs it in the context of the instance" do
        form_class = Class.new(Formulario::Form) do
          field :str, :string, default: proc { default_string.upcase }

          def default_string
            'the default string'
          end
        end

        form = form_class.new

        expect(form.str).to eq(Formulario::Field.for('THE DEFAULT STRING'))
      end

      it "overrides the default even if it's a block default block" do
        form_class = Class.new(Formulario::Form) do
          field :str, :string, default: proc { 'default' }
        end

        form = form_class.new(str: 'hello')

        expect(form.str).to eq(Formulario::Field.for('hello'))
      end
    end

    context "params" do
      it "starts out as an empty hash" do
        form = Formulario::Form.new

        expect(form.params).to eq({})
      end

      it "has all the initialized fields", :preserve_constants do
        ::Slug = Class.new(Formulario::Field)

        form_class = Class.new(Formulario::Form) do
          field :slug, Slug
          field :str, Formulario::Field::String
          field :num, Formulario::Field::Integer
          field :nothing, Formulario::Field::String
        end

        form = form_class.new(str: 'String', num: 42, slug: 'the-slug')

        expect(form.params).to eq({
                                    slug:    'the-slug',
                                    str:     'String',
                                    num:     42,
                                    nothing: nil,
                                  })
      end
    end

    context "defaults" do
      it "returns a form with all the defaults", :preserve_constants do
        ::Slug = Class.new(Formulario::Field)

        form_class = Class.new(Formulario::Form) do
          field :slug, Slug,   default: proc { default_slug }
          field :num, :integer, default: 42
          field :str, :string
          field :raw

          validate :str do |val|
            !val.nil?
          end

          def default_slug
            'a-slug'
          end
        end

        form = form_class.default

        expect(form.slug).to eq(Slug.for('a-slug'))
        expect(form.num).to eq(Formulario::Field.for(42))
        expect(form.str).to eq(::Formulario::Field::Blank.new)
        expect(form.raw).to eq(::Formulario::Field::Blank.new)

        expect(form.valid?).to eql(true)
      end

      it "defaults form fields as form field default", :preserve_constants do
        ::OtherForm = Class.new(::Formulario::Form) do
          field :nested
          field :nested_with_default, default: "DEFAULT VALUE"
        end

        form_class = Class.new(Formulario::Form) do
          field :other, OtherForm
        end

        form = form_class.default

        expect(form.other.class < ::Formulario::Field::FormField).to eql(true)
        expect(form.other.form.class).to eql(OtherForm)

        expect(form.other.nested).to eq(::Formulario::Field::Blank.new)
        expect(form.other.nested_with_default.value).to eq("DEFAULT VALUE")
      end

    end

    # TODO: Remove this
    context 'validations' do
      it "is valid if no validations are present" do
        form = Formulario::Form.new

        expect(form.valid?).to eql(true)
      end

      it "is valid if it passes validations" do
        form_class = Class.new(Formulario::Form) do
          field :int

          validate :int do |val|
            val > 45
          end
        end

        form = form_class.new(int: 150)

        expect(form.valid?).to eql(true)
      end

      it "is invalid if it doesn't pass validations" do
        form_class = Class.new(Formulario::Form) do
          field :int

          validate :int do |val|
            val > 45
          end
        end

        form = form_class.new(int: 1)

        expect(form.valid?).to eql(false)
        expect(form.int.reasons).to eql(['is invalid'])
      end

      it "executes validations on the instance", :preserve_constants do
        ::MyForm = Class.new(Formulario::Form) do
          field :int
          attr_accessor :base

          validate :int do |val|
            val > form.base
          end
        end

        form1 = MyForm.new(int: 1, base: 2)
        expect(form1.int.exceptional?).to eql(true)

        form2 = MyForm.new(int: 5, base: 2)
        expect(form2.int.exceptional?).to eql(false)
      end

      it "allows to set a message" do
        form_class = Class.new(Formulario::Form) do
          field :int

          validate :int, message: 'too small' do |val|
            val > 45
          end
        end

        form = form_class.new(int: 1)

        expect(form.int.reasons).to eql(['too small'])
      end

      it "allows multiple validations on the same field" do
        form_class = Class.new(Formulario::Form) do
          field :int

          validate :int, message: 'too small' do |val|
            val > 45
          end

          validate :int, message: 'too very small' do |val|
            val > 50
          end
        end

        form = form_class.new(int: 1)

        expect(form.int.reasons).to eql(['too small', 'too very small'])
      end

      it "allows multiple validations on different fields" do
        form_class = Class.new(Formulario::Form) do
          field :int
          field :str

          validate :int, message: 'too small' do |val|
            val > 45
          end

          validate :str, message: 'needs to start with the letter m' do |val|
            val.start_with?('m')
          end

          validate :str, message: 'needs to end with the letter m' do |val|
            val.end_with?('m')
          end
        end

        form = form_class.new(int: 1, str: 'master')

        expect(form.int.reasons).to eql(['too small'])
        expect(form.str.reasons).to eql(['needs to end with the letter m'])
      end

      it "takes symbols as method names" do
        field_class = Class.new(Formulario::Field::Integer) do
          def is_bigger(val)
            val > 45
          end
        end

        form_class = Class.new(Formulario::Form) do
          field :int, field_class

          validate :int, :is_bigger, message: 'needs to be greater than 45'

        end

        form = form_class.new(int: 1)

        expect(form.int.reasons).to eql(['needs to be greater than 45'])
      end

      it "accepts a validator", :preserve_constants do
        ::LenghtValidator = Class.new(Formulario::Validator) do
          attr_reader :length

          def initialize(length)
            @length = length
            super()
          end

          def validate(value)
            value.size == length
          end

          def failure_text
            value.size > length ? 'too large' : 'too short'
          end
        end

        form_class = Class.new(Formulario::Form) do
          field :str

          validate :str, LenghtValidator.new(5)
        end

        form = form_class.new(str: 'a')

        expect(form.valid?).to eql(false)
        expect(form.str.exceptional?).to eql(true)
        expect(form.str.reasons).to eql(['too short'])
      end
    end

    context "errors" do
      it "does something" do
        form_class = Class.new(Formulario::Form) do

          field :field1
          field :field2
          field :field3
          field :field4
          field :field5

          validate(:field1, message: 'reason 1') { false }
          validate(:field2, message: 'reason 2') { true  }
          validate(:field3, message: 'reason 3') { false }
          validate(:field4, message: 'reason 4') { true  }
          validate(:field5, message: 'reason 5') { false }
        end

        form = form_class.new

        expect(form.error_fields.keys).to match_array([:field1, :field3, :field5])
        expect(form.error_fields.values.map(&:reasons)).to match_array([
                                                                         ['reason 1'],
                                                                         ['reason 3'],
                                                                         ['reason 5'],
                                                                       ])
      end

      it "accepts nested forms", :preserve_constants do
        ::ProfileForm = Class.new(Formulario::Form) do
          field :avatar, :string
        end

        ::UserForm = Class.new(Formulario::Form) do
          field :email, :string
          field :profile, ::Formulario::Field::FormField[ProfileForm]
        end

        form = UserForm.new(email: 'EMAIL', profile: { avatar: "AVATAR"})

        expect(form.email).to eq(::Formulario::Field::String.for("EMAIL"))
        expect(form.profile.avatar).to eq(::Formulario::Field::String.for("AVATAR"))
      end

      it "accepts nested forms", :preserve_constants do
        ::ProfileForm = Class.new(Formulario::Form) do
          field :avatar, :string
        end

        ::UserForm = Class.new(Formulario::Form) do
          field :profile, ProfileForm
        end

        form = UserForm.new(profile: { avatar: "AVATAR"})

        expect(form.profile.avatar).to eq(::Formulario::Field::String.for("AVATAR"))
      end

      it "validates nested forms properly", :preserve_constants do
        ::ProfileForm = Class.new(Formulario::Form) do
          field :avatar, :string

          validate :avatar, message: 'NOPE' do
            false
          end
        end

        ::UserForm = Class.new(Formulario::Form) do
          field :email, :string
          field :profile, ProfileForm
        end

        form = UserForm.new(email: 'EMAIL', profile: { avatar: "AVATAR"})

        expect(form.valid?).to eq(false)
        expect(form.profile.avatar.exceptional?).to eql(true)
        expect(form.profile.avatar.reasons).to eq(['NOPE'])
      end

      it "allows for a custom exceptional field", :preserve_constants do
        class ::MyExceptionalValue < Formulario::Field::ExceptionalValue
          def self.for(raw_value, reasons: Formulario::Undefined)
            super
          end
        end

        field_class = Class.new(Formulario::Field) do
          def exceptional_class
            ::MyExceptionalValue
          end
        end

        form_class = Class.new(Formulario::Form) do
          field :my_val, ::MyExceptionalValue

          validate :my_val, message: 'NOPE' do
            false
          end

        end

        form = form_class.new(my_val: "VALUE")

        expect(form.my_val.exceptional?).to eql(true)
        expect(form.my_val).to be_a(::MyExceptionalValue)
      end

      it "ignores extra field keys" do
        form_class = Class.new(Formulario::Form) do
          field :existing_field
        end

        form = form_class.new(existing_field: "I EXIST", extra_field: "I DON'T")

        expect { form.extra_field }.to raise_error(NoMethodError)
      end

      it "can raise an error if extra field keys are passed in" do
        skip("Make this work")

        form_class = Class.new(Formulario::Form) do
          # TODO: make this name better
          raise_on_extra_keys!
          field :existing_field
        end

        expect {
          form_class.new(existing_field: "I EXIST", extra_field: "I DON'T")
        }.to raise_error("key not found?")
      end

    end
  end
