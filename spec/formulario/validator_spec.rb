RSpec.describe Formulario::Validator do
  it "needs to be validated" do
    validator = Formulario::Validator.new

    expect { validator.valid? }.to raise_error(Formulario::Validator::ValidationNotPerformed)
  end

  it "is valid by default" do
    validator = Formulario::Validator.new

    validator.call(value: nil)

    expect(validator.valid?).to eql(true)
  end

  it "is valid if validation passes" do
    validator_class = Class.new(Formulario::Validator) do
      def validate(value)
        true
      end
    end

    validator = validator_class.new

    validator.call(value: nil)

    expect(validator.valid?).to eql(true)
  end

  it "is invalid if validation doesn't pass" do
    validator_class = Class.new(Formulario::Validator) do
      def validate(value)
        false
      end
    end

    validator = validator_class.new

    validator.call(value: nil)

    expect(validator.valid?).to eql(false)
  end

  it "can be initialized with a block" do
    validator = Formulario::Validator.new { |value| value > 5}

    validator.call(value: 3)

    expect(validator.valid?).to eql(false)
  end

  it "retunrs self from the call method if it is valid" do
    validator = Formulario::Validator.new

    expect(validator.call(value: nil)).to eql(validator)
  end

  it "has access to the value" do
    validator_class = Class.new(Formulario::Validator) do
      attr_reader :length

      def validate(value)
        value == 5
      end
    end

    validator = validator_class.new

    validator.call(value: 5)

    expect(validator.valid?).to eql(true)
  end

  it "allows to set a custom failure message" do
    validator_class = Class.new(Formulario::Validator) do
      def validate(value)
        false
      end

      def message
        "#{field_name}: #{value.inspect} is invalid for #{object}"
      end
    end

    validator = validator_class.new

    validator.call(value: 5, field_name: :test_field, object: :the_object)

    expect(validator.message).to eql('test_field: 5 is invalid for the_object')
  end

  it "returns an empty message if valid" do
    validator_class = Class.new(Formulario::Validator) do
      def failure_text
        "#{field_name}: #{value.inspect} is invalid for #{object}"
      end
    end

    validator = validator_class.new

    validator.call(value: 5, field_name: :test_field, object: :the_object)

    expect(validator.valid?).to eql(true)
    expect(validator.message).to eql('')
  end

  it "runs the on_valid block if valid" do
    validator_class = Class.new(Formulario::Validator) do
      def validate(value)
        value
      end
    end

    @valid   = Formulario::Undefined
    @invalid = Formulario::Undefined

    validator = validator_class.new

    validator.call(value: true).on_valid {
      @valid = true
    }.on_invalid {
      @invalid = true
    }

    expect(@valid).to eql(true)
    expect(@invalid).to eql(Formulario::Undefined)
  end

  it "runs the on_invalid block if invalid" do
    validator_class = Class.new(Formulario::Validator) do
      def validate(value)
        value
      end
    end

    @valid   = Formulario::Undefined
    @invalid = Formulario::Undefined

    validator = validator_class.new

    validator.call(value: false).on_valid {
      @valid = true
    }.on_invalid {
      @invalid = true
    }

    expect(@valid).to eql(Formulario::Undefined)
    expect(@invalid).to eql(true)
  end
end
