def m(o)
  (o.methods-Object.new.methods).sort
end

def pm(o)
  ap m(o)
end
alias :apm :pm

require "bundler/setup"

require "awesome_print"
AwesomePrint.defaults = {
  indent: 2,
  index:  false,
}

require "formulario"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.around(:each) do |example|
    if example.metadata[:preserve_constants]
      begin
        constants = ::Object.constants
        example.run
      ensure
        (::Object.constants - constants).each do |klass|
          ::Object.send(:remove_const, klass)
        end
      end
    else
      example.run
    end
  end

  config.before do |example|
    if example.metadata[:migrating]
      message = ""
      message << "\n # "
      message << example.location
      message << ":Migrating"

      if example.metadata[:migrating].is_a?(String)
        message << " to\n".purple
        message << "  # ".blue
        message << example.metadata[:migrating].blue
      end

      puts message.purple
    end
  end
end
